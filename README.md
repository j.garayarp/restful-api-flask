# RESTful-API-Flask

A RESTful API with Python using the micro-framework flask. This project simulates a library and allows to classify books. The books have categories and authors respectively.

## v1.0
Using PostgreSQL I build a DB with 3 tables (authors, books, categories). The Restful API allows me some functionalities:
* List categories
* Create author
* list authors (alphabetic order)
* Detail author
* Create a book
* Detail book
* Update book
* Delete book

## v1.1
Added a table user and functionalities to authenticate:
* Login generates a jwt token
* Decorator to validate token
* Get all users
* Get on user
* Create user, generating a hash sha256 for password.
* Update user
* Delete user