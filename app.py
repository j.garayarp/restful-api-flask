# FLask
from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy

# Security
import uuid
from werkzeug.security import generate_password_hash, check_password_hash

# JSON Web Token
import jwt

# Tools
import datetime
from functools import wraps

# Credentials
from credentials import Credentials


app = Flask(__name__)

app.config['SECRET_KEY'] = Credentials.secret_key
app.config['ENV']='development'
app.config['SQLALCHEMY_DATABASE_URI'] = Credentials.database_v2
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(50), unique=True)
    name = db.Column(db.String(50))
    password = db.Column(db.String(80))
    admin = db.Column(db.Boolean)

class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    age = db.Column(db.Integer, db.CheckConstraint('age>0'))
    books = db.relationship('Book', backref='author')

class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), unique=True, nullable=False)
    books = db.relationship('Book', backref='category')

class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    editorial = db.Column(db.String(50))
    author_id = db.Column(db.Integer, db.ForeignKey('author.id'), nullable=False)
    category_id = db.Column(db.Integer, db.ForeignKey('category.id'), nullable=False)


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if 'x-access-token' in request.headers:
            token = request.headers['x-access-token']
        if not token:
            return jsonify({'message' : 'Token is missing'}), 401
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message' : 'Token is invalid!'}), 401
        return f(current_user, *args, **kwargs)
    return decorated


@app.route('/login')
def login():
    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})
    user = User.query.filter_by(name=auth.username).first()
    if not user:
        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})
    if check_password_hash(user.password, auth.password):
        token = jwt.encode({'public_id' : user.public_id, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=30)}, app.config['SECRET_KEY'])
        return jsonify({'token' : token.decode('UTF-8')})
    return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

@app.route('/users', methods=['GET'])
@token_required
def get_all_users(current_user):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    users = User.query.all()
    output = []
    for user in users:
        user_data = {}
        user_data['public_id'] = user.public_id
        user_data['name'] = user.name
        user_data['password'] = user.password
        user_data['admin'] = user.admin
        output.append(user_data)
    return jsonify({'users' :output})

@app.route('/users/<public_id>', methods=['GET'])
@token_required
def get_one_user(current_user,public_id):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message':'No user found!'})
    user_data = {}
    user_data['public_id'] = user.public_id
    user_data['name'] = user.name
    user_data['password'] = user.password
    user_data['admin'] = user.admin
    return jsonify({'user' : user_data})

@app.route('/users', methods=['POST'])
@token_required
def create_user(current_user):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    data = request.get_json()
    hashed_password = generate_password_hash(data['password'], method='sha256')
    new_user = User(public_id=str(uuid.uuid4()), name=data['name'], password=hashed_password, admin=data['admin'])
    db.session.add(new_user)
    db.session.commit()
    return jsonify({'message' : 'New user created!'})

@app.route('/users/<public_id>', methods=['PUT'])
@token_required
def update_user(current_user,public_id):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message':'No user found!'})
    data = request.get_json()
    hashed_password = generate_password_hash(data['password'], method='sha256')
    user.public_id = str(uuid.uuid4())
    user.name = data['name']
    user.password = hashed_password
    user.admin = data['admin']
    db.session.commit()
    return jsonify({'message': 'The user has been updated'})

@app.route('/users/<public_id>', methods=['DELETE'])
@token_required
def delete_user(current_user,public_id):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    user = User.query.filter_by(public_id=public_id).first()
    if not user:
        return jsonify({'message':'No user found!'})
    db.session.delete(user)
    db.session.commit()
    return jsonify({'message': 'The user has been deleted'})


@app.route('/categories', methods=['POST'])
@token_required
def create_category():
    data = request.get_json()
    new_category = Category(name=data['name'])
    db.session.add(new_category)
    db.session.commit()
    return jsonify({'message' : 'New category added'})

@app.route('/categories', methods=['GET'])
def list_categories():
    categories = Category.query.all()
    output = []
    for category in categories:
        category_data = {}
        category_data['name'] = category.name
        category_data['id'] = category.id
        output.append(category_data)
    return jsonify({'categories' : output})


@app.route('/authors', methods=['POST'])
@token_required
def create_author(current_user):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    data = request.get_json()
    
    new_author = Author(first_name=data['first_name'],last_name=data['last_name'],age=data['age'])
    db.session.add(new_author)
    db.session.commit()
    return jsonify({'message' : 'New author added'})

@app.route('/authors', methods=['GET'])
def list_authors():
    authors = Author.query.order_by('last_name').add_columns('id','last_name')
    output = []
    for author in authors:
        author_data = {}
        author_data['id'] = author.id
        author_data['last_name'] = author.last_name
        output.append(author_data)
    return jsonify({'authors' : output})

@app.route('/authors/<author_id>', methods=['GET'])
def detail_author(author_id):
    author = Author.query.get(author_id)
    if not author:
        return jsonify({'message':'No author found!'})
    author_data = {}
    author_data['id'] = author.id
    author_data['first_name'] = author.first_name
    author_data['last_name'] = author.last_name
    author_data['age'] = author.age
    return jsonify({'author' : author_data})


@app.route('/books', methods=['POST'])
@token_required
def create_book(current_user):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    data = request.get_json()
    new_book = Book(title=data['title'],editorial=data['editorial'],author_id=data['author_id'],category_id=data['category_id'])
    db.session.add(new_book)
    db.session.commit()
    return jsonify({'message':'New book created'})

@app.route('/books/<book_id>', methods=['GET'])
def detail_book(book_id):
    book = Book.query.get(book_id)
    if not book:
        return jsonify({'message':'No book found!'})
    book_data = {}
    book_data['first_name'] = book.author.first_name
    book_data['last_name'] = book.author.last_name
    book_data['title'] = book.title
    book_data['category'] = book.category.name

    return jsonify({'book' : book_data})

@app.route('/books', methods=['GET'])
def list_books():
    books = Book.query.all()
    output = []
    for book in books:
        book_data = {}
        book_data['id'] = book.id
        book_data['title'] = book.title
        book_data['category'] = book.category.name
        book_data['first_name_author'] = book.author.first_name
        book_data['last_name_author'] = book.author.last_name
        output.append(book_data)
    return jsonify({'books':output})

@app.route('/books/<book_id>', methods=['DELETE'])
@token_required
def delete_book(current_user,book_id):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    book = Book.query.get(book_id)
    if not book:
        return jsonify({'message':'No user found!'})
    db.session.delete(book)
    db.session.commit()
    return jsonify({'message': 'The book has been deleted'})

@app.route('/books/<book_id>', methods=['PUT'])
@token_required
def update_book(current_user,book_id):
    if not current_user.admin:
        return jsonify({'message' : 'Cannot perform that function!'})
    book = Book.query.get(book_id)
    if not book:
        return jsonify({'message':'No user found!'})
    data = request.get_json()
    book.title = data['title']
    book.editorial = data['editorial']
    book.author_id = data['author_id']
    book.category_id = data['category_id']
    db.session.commit()
    return jsonify({'message': 'The book has been updated'})


if __name__ == '__main__':
    app.run(debug=True)